'''A function takes any number and caclulates the product and sum of its individual digits '''

def num_product(any_num):
    temp_list = list(str(any_num))
    anytemplist = [int(x) for x in temp_list]
    def product_sum(anytemplist): 
        prod_total = 1 
        for count, value in enumerate(anytemplist):
            prod_total *= anytemplist[count]
        return prod_total
    product_list = product_sum(anytemplist)
    return sum(anytemplist), product_list