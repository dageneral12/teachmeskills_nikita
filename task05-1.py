#Hw5 
'''The function to take in the values and the operands'''

def calculator(x, y, operand): 
    operands = ['*', '/', '-', '+']
    if y == 0 and operand == '/': 
        print("You can't divide by 0!")
        return 0
    elif operand not in operands: 
        print("Wrong operand!")
        return 0
    else: 
        if operand == '*': 
            return x * y 
        if operand == '/':
            return x / y 
        if operand == '-': 
            return x - y 
        if operand == '+': 
            return x + y
     
'''The infinite loop with a breakpoint at '0' '''     
while True: 
    print('Select operand = 0 to quit')
    x = int(input('Enter value for x: '))
    y = int(input('Enter value for y: '))
    operand = input('Enter operand (*, /, -, +)')
    if operand == '0': 
        break 
    else: 
        print(calculator(x, y, operand))