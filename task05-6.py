'''The function below identifies a number of list members, which are followed by an increasing number''' 

list_increasing = [1,2,1,3,5,6,5,1,4]

def check_inc_sequence(list_increasing): 
    counter = 0
    for i in range(len(list_increasing)-1):
        if list_increasing[i+1] > list_increasing[i]: 
            counter += 1
    
    return f'The number of strictly increasing numbers is {counter}'


check_inc_sequence(list_increasing)


'''sample output: 
​
'The number of strictly increasing numbers is 5' '''


