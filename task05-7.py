

'''Replacement of numbers in a matrix''' 


matrix = [[1, 2, 3, 4], 
          [5, 6, 7, 8], 
          [9, 10, 11, 12], 
          [13, 14, 15, 16]]

'''The function takes a matrix as an argument, identifies a max number in each row 
and returns a list with max numbers''' 

def get_max_numbers(matrix): 
    max_val = 0
    coord_x = 0
    coord_y = len(matrix[0])
    some_list = []

    for i in matrix:
        max_val = max(i)
        some_list.append(max_val)
    
    return some_list
    

'''The function takes the initial matrix and the list with max numbers 
  and replaces the numbers diagonally with max numbers''' 
        
def insert_nums(some_list, matrix): 
    counter1 = len(matrix[0])-1
    counter2 = 0
    ff_list = []
    for i in matrix:
        
        i.insert(some_list[counter2], i[counter1])
        i.pop(counter1)
        counter1 -= 1 
        counter2 += 1

 
            
    return matrix
            
            
some_list = get_max_numbers(matrix)

insert_nums(some_list, matrix)


'''sample output: 

[[1, 2, 3, 4], [5, 6, 8, 7], [9, 11, 12, 10], [14, 15, 16, 13]]'''