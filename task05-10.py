
'''The function below uses the datetime library to filter out the trains, 

where the difference between departure and arrival time is greater than 7 hrs and 20 mins ''' 

'''importing libraries''' 

from datetime import datetime, timedelta

'''sample dictionary with the timetable'''


train_timetable = {'1234': ('Moscow', '11:45:00', 'St.Petersburg', '15:55:00'),
                   '2345': ('Ryazan', '12:15:00', 'Moscow', '19:05:00'),
                    '3456': ('Moscow', '10:45:00', 'Vitebsk', '21:10:00'), 
                    '8754': ('St. Petersburg', '14:40:00', 'Tallinn', '23:40:00')}
                    
'''The function caclulates the delta between departure and arrival and checks if it's greater than a given interval (7hr 20mins), 

any trains, which travel longer than that are returned as a dictionary object''' 


def check_trains(train_timetable): 
    
    filtered_dict = {}
    dep_time = 0 
    arr_time = 0
    format_str = '%H:%M:%S'

    time_delta = timedelta(days = 0, hours = 7, minutes = 20)

    for k, v in train_timetable.items():
        dep_time = datetime.strptime(v[1], format_str)
        arr_time = datetime.strptime(v[3], format_str)
        diff_time = arr_time - dep_time
        if diff_time > time_delta: 
            filtered_dict[k] = v
    
    return filtered_dict
     

check_trains(train_timetable)


'''sample output: 

{'3456': ('Moscow', '10:45:00', 'Vitebsk', '21:10:00'),
 '8754': ('St. Petersburg', '14:40:00', 'Tallinn', '23:40:00')}''' 
