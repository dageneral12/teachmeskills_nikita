

'''A function below finds all divisors of a given list of numbers and returns them as a dictionary ''' 

def find_divisor(m, n): 
    empty_dict = {}
    empty_list = []
    list_range = list(range(m, n+1))
    for i in list_range: 
        x = i 
        while x != 0:
            if i % x == 0: 
                empty_list.append(x)
            x -= 1 
        empty_dict[i] = empty_list
        empty_list = []
    return empty_dict
            
find_divisor(100, 105)

'''sample output: 


{100: [100, 50, 25, 20, 10, 5, 4, 2, 1],
 101: [101, 1],
 102: [102, 51, 34, 17, 6, 3, 2, 1],
 103: [103, 1],
 104: [104, 52, 26, 13, 8, 4, 2, 1],
 105: [105, 35, 21, 15, 7, 5, 3, 1]}'''