'''The function reverses the order of the words in a list''' 


sample_list = ['foo', 'bar', 'la', 'blah', 'moo']



def reverse_list1(sample_list): 
    other_l = []
    for i in sample_list[::-1]: 
        other_l.append(i)
        
    return other_l
        
reverse_list1(sample_list)

'''sample output: 

['moo', 'blah', 'la', 'bar', 'foo']