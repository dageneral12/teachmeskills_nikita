
'''A function takes a number, from which a list of terms is created

It then divides 1 by each term and adds it to a list, after which it returns a sum of all numbers in the list''' 


def div_sum(num_to_check): 
    empt_list = []
    num_list = list(range(1, num_to_check))
    for i in num_list:
        i = int(i)
        n = 1/i
        empt_list.append(n)
    return sum(empt_list)
    
num_to_check = int(input('Enter a max term number: '))
print(div_sum(num_to_check))

