''' The function takes an array of numbers, identifies the highest number and uses it 
to replace every second number in the initial array''' 



def replace_arr(array_to_check): 
    other_arr = []
    other_arr = array_to_check
    l = int(sorted(other_arr)[-1])
    array_to_check = [int(x) for x in array_to_check]
    for count, value in enumerate(array_to_check): 
        if value % 2 == 0: 
            array_to_check.insert(count, l)
            array_to_check.pop(count+1)

    return array_to_check

array_to_check = [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19]

replace_arr(array_to_check)


'''sample output: 


[1, 19, 3, 19, 5, 19, 7, 19, 9, 19, 11, 19, 13, 19, 15, 19, 17, 19, 19]'''
