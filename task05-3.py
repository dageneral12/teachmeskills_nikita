'''Amicable numbers'''



'''This function creates and returns a dictionary with a list of numbers as keys and all of their divisors as values'''

def num_div(list_to_check): 
    list_div = []
    sample_dict = {}
    for i in list_to_check:
        j = i
        while j != 0: 
            if i % j == 0 and i != j: 
                list_div.append(j)
            j -= 1 
        sample_dict[i] = list_div
        list_div = []
    return sample_dict
            
            
            
    
    
'''This function takes the result of function num_div and searches for a matching pair of amicable numbers, which are added to another dictionary'''


def sum_nums(some_dict): 
    final_list = {}
    for key, value in some_dict.items():
        num1 = sum(value)
        if num1 in some_dict.keys(): 
            num2 = sum(some_dict[num1])
            if sum(some_dict[num1]) == num2: 
                final_list[key] = num1
    return final_list
        
    
'''Creating the range of numbers and calling the respective functions below''' 
    
list_to_check = list(range(200, 301))
some_dict = num_div(list_to_check)
final_list = sum_nums(some_dict)
print(final_list)

'''sample output: 

{200: 265, 204: 300, 208: 226, 220: 284, 222: 234, 224: 280, 230: 202, 232: 218, 246: 258, 248: 232, 
250: 218, 256: 255, 258: 270, 266: 214, 268: 208, 272: 286, 282: 294, 284: 220, 286: 218, 290: 250, 292: 226, 296: 274}'''
